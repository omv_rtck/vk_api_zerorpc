# -*- coding: utf-8 -*-

# related third party imports
import zerorpc
import vk_api

class VkApiRPC(object):
    def __init__(self, login, password, app_id=-1):
        try:
            if app_id == -1:
                self.api = vk_api.VkApi(login, password)
            else:
                self.api = vk_api.VkApi(login, password, app_id)

            self.api.authorization() # Авторизируемся

        except vk_api.AuthorizationError as error_msg:
            print(error_msg)
            return None

    def reg_stop(self,proc):
        self.stop_proc = proc

    def rpc_stop(self):
        if self.stop_proc:
            self.stop_proc()

    def method(self, *args):
        return self.api.method(*args)
